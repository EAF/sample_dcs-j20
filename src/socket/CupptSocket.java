package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;


import thread.ObservableListener;

public class CupptSocket {

	private Socket socket;

	private String socketIP;
	private int socketPort;
	private String socketName;

	private int messageID;

	// for I/O
	private BufferedReader socketIn = null; // to write on the socket
	private PrintWriter socketOut = null; // to read from the socket

	private ObservableListener socketListener;
	private Thread socketTListener;

	private String interfaceMode;
	private boolean locked;

	public CupptSocket() {
		socket = new Socket();
	}

	public CupptSocket(String IP, int Port, String Name) throws UnknownHostException, IOException {
		socket = new Socket(IP, Port);

		socketIP = IP;
		socketPort = Port;
		socketName = Name;

		socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
		socketOut = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));


	}

	public Socket getSocket() {
		return this.socket;
	}

	public BufferedReader getInput() {
		return this.socketIn;
	}

	public PrintWriter getOutput() {
		return this.socketOut;
	}

	public String getIP() {
		return this.socketIP;
	}

	public int getPort() {
		return this.socketPort;
	}

	public String getName() {
		return this.socketName;
	}
	
	public String getInterfaceMode() {
		return this.interfaceMode;
	}

	public int getMessageID() {
		return this.messageID;
	}

	public ObservableListener getObservableListener() {
		return this.socketListener;
	}

	public boolean isLocked() {
		return this.locked;
	}

	// setters

	public String setInterfaceMode(String dInterfaceMode) {
		return this.interfaceMode = dInterfaceMode;
	}

	public void setObservableListener(ObservableListener obsListener) {
		this.socketListener = obsListener;

		socketTListener = new Thread(this.socketListener);
		socketTListener.start();
	}

	public boolean setLock(boolean dLocked) {
		return this.locked = dLocked;
	}

	// others

	public void send(String message) {
		socketOut.write(message); // write message
		socketOut.flush(); // send message

	}

	public void disconnect() {
		try {

			if (socket != null) {
				System.out.println("Stop socket Socket");
				socket.close();
			}

			if (socketIn != null) {
				System.out.println("Stop socket Reading");
				socketIn.close();
			}

			if (socketOut != null) {
				System.out.println("Stop socket Writing");
				socketOut.close();
			}

		} catch (Exception e) {
			System.out.println("exception");

		}
	}

	public String toString() {
		return "socket " + socketIP + ":" + socketPort;
	}

	public void incrementMessageID() {

		messageID++;

	}
}